Hooks.on('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'lang-de-pf1',
            lang: 'de',
            dir: 'translations'
        });
    }
});

